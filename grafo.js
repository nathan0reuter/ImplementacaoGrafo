 /** Nathan Reuter Godinho - 11202892
 *  Disciplina de Grafos
 *  Implementação de Estrutura de Grafos
 */

'use strict';

var Grafo = function () {

    var totalArestas = 0,

        limpaGrafo,

        vertices = {},

        /** Operações Básicas */

        /**
         * Adiciona vertice aos vertices
         * @params verticeID: String
         */
        adicionaVertice,

        /**
         * Remove o vertice vertice dos vertices
         * @params verticeID: String
         */
        removeVertice,

        /**
         * Conecta os vértices idVertice1 e idVertice2 em G
         * @params verticeID1: String, verticeID: String, pesoAresta(opcional)
         */
        conectaVertice,

        /** Desconecta os vértices v1 e v2 em G
         * @params verticeID: String, verticeID: String
         */
        desconectaVertice,

        /** Retorna o número de vértices de G
         * @return number
         */
        ordem,

        /** Retorna um vértice qualquer de G
         */
        umVertice,

        /** Retorna um conjunto contendo os vértices adjacentes a v em G
         */
        adjacentes,

        /** Retorna o número de vértices adjacentes a v em G
         * @return number
         */
        grau,

        /** Retorna nome dos vertices
          * @return boolean
          */
        nomeDosVertices,

        /** Derivadas */

        /** Verifica se todos os vértices de G possuem o mesmo grau
         * @return boolean
         */
        eRegular,

         /** Verifica se cada vértice de G está conectados
          *      a todos os outros vértices
          * @return boolean
          */
        eCompleto,

         /** Verifica se existe pe  lo menos um caminho que entre
          *      cada par de vértices de G
          * @return boolean
          */
        eConexo,

        buscaEmProdundidade,

          /** Verifica se não há ciclos em G
          * @return boolean
          */
        temCiclos;


    return {
        totalArestas: totalArestas,

        vertices: vertices,

        limpaGrafo: function () {
            Grafo.vertices = {};
            Grafo.totalArestas = 0;
        },

        adicionaVertice: function (verticeID) {
            if (verticeID) {
                var verticeParaAdicionar = {
                    id: verticeID,
                    arestas: {}
                };

                Grafo.vertices[verticeID] = verticeParaAdicionar;
            } else {
                console.error('Vertice nulo ou sem id');
            }

        },

        removeVertice: function (vertice) {
            if (vertice) {
                var arestasDoVertice = Object.keys(Grafo.vertices[vertice].arestas);

                arestasDoVertice.forEach(function (verticeAdj) {
                    Grafo.desconectaVertice(vertice, verticeAdj);
                });

                delete Grafo.vertices[vertice];
            }
        },

        conectaVertice: function (verticeID1, verticeID2, pesoAresta) {
            if (verticeID1 && verticeID2) {
                Grafo.vertices[verticeID1].arestas[verticeID2] = {id: verticeID2, pesoAresta: pesoAresta};
                Grafo.vertices[verticeID2].arestas[verticeID1] = {id: verticeID1, pesoAresta: pesoAresta};
                Grafo.totalArestas++;
            }
        },

        desconectaVertice: function (vertice1, vertice2) {
            if (vertice1 && vertice2) {
                delete Grafo.vertices[vertice1].arestas[vertice2];
                delete Grafo.vertices[vertice2].arestas[vertice1];
                Grafo.totalArestas--;
            }
        },

        ordem: function () {
            return Object.keys(Grafo.vertices).length;
        },

        umVertice: function () {
            var arrayIdVertices = Object.keys(vertices),
                numeroAletorio = Math.round(Math.random() * arrayIdVertices.length),
                idSorteado = arrayIdVertices[numeroAletorio];

            return Grafo.vertices[idSorteado];
        },

        adjacentes: function (verticeId) {
            if (verticeId) {
                var arestasDoVertice = vertices[verticeId].arestas,
                    adjacentesDeV = [];


                Object.keys(arestasDoVertice).forEach(function (adjacenteID) {
                    adjacentesDeV.push(vertices[adjacenteID]);
                });

                return adjacentesDeV;
            }
        },

        grau: function (verticeID) {
            var vertice = verticeID && Grafo.vertices[verticeID];

            if (vertice) {
                return Object.keys(vertice.arestas).length;
            }

            return new Error('Vertice Inexistente');
        },

        nomeDosVertices: function () {
            return Object.keys(Grafo.vertices);
        },

        eRegular: function () {
            var idVertices = Object.keys(Grafo.vertices),
                ordemAtual = Grafo.grau(Object.keys(Grafo.vertices)[0]);

            return idVertices.every(function (vertice) {

                if (ordemAtual !== Grafo.grau(vertice)) {
                    return false;
                }

                return true;
            });
        },

        eCompleto: function () {
            if (Grafo.totalArestas >= Grafo.ordem() - 1) {
                var nomeVertices = Grafo.nomeDosVertices();

                return nomeVertices.every(function (idvertice) {

                    var nomeArestas = Object.keys(Grafo.vertices[idvertice].arestas),
                        compareArray = nomeVertices.slice();

                    compareArray.splice(compareArray.indexOf(idvertice), 1);

                    return nomeArestas.equals(compareArray);
                });
            }

            return false;
        },

        buscaEmProdundidade: function (foiVisitadoCB) {
            // Buscar varre totalmente uma component conexa
            var buscar = function (algumVertice) {
                var verticesVisitados = [],
                    pilha = [],
                    primeiroVertice = algumVertice;

                pilha.push(primeiroVertice);

                while (pilha.length !== 0) {
                    var idVertice = pilha.pop();

                    // Caso o vertice não tenha sido visitado
                    if (verticesVisitados.indexOf(idVertice) === -1) {
                        verticesVisitados.push(idVertice);

                        Object.keys(Grafo.vertices[idVertice].arestas).forEach(function (vertice) {
                            if (verticesVisitados.indexOf(vertice) === -1) {
                                pilha.push(vertice);
                            }
                        });
                        // Função foiVisitadoCB pode ser chamada por outros metodos quando
                        // vertice ja foi visitado e ser executada.
                    } else {
                        if (foiVisitadoCB && foiVisitadoCB()) {
                            totalVisitados = Grafo.nomeDosVertices;
                            break;
                        }
                    }
                }

                return verticesVisitados;
            },

            totalVisitados = [];

            totalVisitados = buscar(Grafo.nomeDosVertices()[0]);

            /* Caso o buscar não tenha passado por todos os vertices, ele é chamado de novo para
              outros outros vertices não visitados até passar por todos **/
            while (totalVisitados.length < Grafo.nomeDosVertices().length) {
                var novoVerticeInicial = Grafo.nomeDosVertices().diff(totalVisitados)[0];
                totalVisitados = totalVisitados.concat(buscar(novoVerticeInicial));
            }

            return totalVisitados;
        },

        temCiclos: function () {
            var totalDeVertices = Grafo.nomeDosVertices().length,
                result = false;

            Grafo.buscaEmProdundidade(function () {
                result = true;
                return true;
            });

            return result;

        }
    };
}();

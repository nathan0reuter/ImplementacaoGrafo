/** Nathan Reuter Godinho - 11202892
 *  Disciplina de Grafos
 *  Implementação de Estrutura de Grafos
 */

'use strict';
var app = {
    gerarGrafoAleatorio: function (maxGrafos) {
        maxGrafos ? maxGrafos = Number(maxGrafos) : maxGrafos = 15;

        for (var i = 1; i < maxGrafos + 1; i++) {
            Grafo.adicionaVertice(i);
        }

        for (var i = 1; i < maxGrafos * 1.5; i++) {
            var vertices = Object.keys(Grafo.vertices),
                numeroAletaorio = function (max) {
                    return Math.round(Math.random() * max + 0);
                };

            Grafo.conectaVertice(vertices[numeroAletaorio(vertices.length)],
                vertices[numeroAletaorio(vertices.length)], numeroAletaorio(1000));
        }

    },

    mapeiaNomeGrafoParaVIS: function (nomeGrafosGerados) {
        var container = [];
        nomeGrafosGerados.forEach(function (grafo) {
            container.push({
                id: grafo,
                label: grafo,
                color: '#21D021'
            });
        });

        return container;
    },

    mapeiaLigacoesParaVIS: function (vertices, ids) {
        var container = [],
            idsQueJaForam = [];

        ids.forEach(function (id) {
            var arestas = Object.keys(vertices[id].arestas);
                idsQueJaForam.push(id);

                arestas.forEach(function (vertice) {
                    if(idsQueJaForam.indexOf(vertice) !== -1) {
                        container.push({
                            from: id,
                            to: vertice,
                            label: vertices[id].arestas[vertice].pesoAresta
                        });
                    }
                });
        });

        return container;
    },

    inicializaVIS: function () {
        app.gerarGrafoAleatorio(controlador.pegaValorDoCampo());
        var nomeDosVertices = Grafo.nomeDosVertices(),
            nodes = new vis.DataSet(app.mapeiaNomeGrafoParaVIS(nomeDosVertices)),
            edges = new vis.DataSet(app.mapeiaLigacoesParaVIS(Grafo.vertices, nomeDosVertices)),
            container = document.getElementById('mynetwork'),
            data = {
                nodes: nodes,
                edges: edges
            },
            options = {},
            network = new vis.Network(container, data, options);
    },

    carregaInformaçõesDoGrafo: function () {
        controlador.adicionaElementoComValor('campo-ordem-grafo', Grafo.ordem());
        controlador.adicionaElementoComValor('campo-totalaresta-grafo', Grafo.totalArestas);
        controlador.adicionaElementoComValor('campo-regular-grafo', (Grafo.eRegular() ? 'Sim' : 'Não'));
        controlador.adicionaElementoComValor('campo-completo-grafo', (Grafo.eCompleto() ? 'Sim' : 'Não'));
        controlador.adicionaElementoComValor('campo-tem-ciclos', (Grafo.temCiclos() ? 'Sim' : 'Não'));
//        controlador.adicionaSeletoresPorVertice('seletor-de', Grafo.nomeDosVertices());
//        controlador.adicionaSeletoresPorVertice('seletor-para' ,Grafo.nomeDosVertices());

    },

    recarregaGrafo: function () {
        Grafo.limpaGrafo();
        app.inicializaVIS();
        app.carregaInformaçõesDoGrafo();
    },

    acharGrafoCompleto: function () {
        while(true){
            app.recarregaGrafo();

            if (Grafo.eCompleto()) {
                break;
            }
        }
    }
};

app.inicializaVIS();
app.carregaInformaçõesDoGrafo();

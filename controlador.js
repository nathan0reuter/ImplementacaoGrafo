'use strict';

var controlador = function () {
    var adicionaElementoComValor,
        pegaValorDoCampo,
        adicionaSeletoresPorVertice;

    return {
        adicionaElementoComValor: function (elemento, valor) {
            document.getElementById(elemento).innerText = valor;
        },

        pegaValorDoCampo: function () {
            return document.getElementsByName('numeroDeVertices')[0].value;
        },

        adicionaSeletoresPorVertice: function (seletor ,nomeVetores) {
            var selecionado = document.getElementsByName(seletor)[0];

                if(selecionado.childNodes.length > 1) {
                    var array = Array.prototype.slice.call(selecionado.childNodes);
                    
                    array.shift();
                    array.forEach(function (opt) {
                        selecionado.removeChild(opt);
                    });
                }

                nomeVetores.forEach( function (nome) {
                    var option = document.createElement("option");
                    option.text = nome;
                    option.value = nome;

                    selecionado.appendChild(option);
                });
        }
    }
}();
